Source: python-automaton
Section: python
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Thomas Goirand <zigo@debian.org>,
 Corey Bryant <corey.bryant@canonical.com>,
Build-Depends:
 debhelper-compat (= 10),
 dh-python,
 openstack-pkg-tools,
 python3-all,
 python3-pbr,
 python3-setuptools,
 python3-sphinx,
Build-Depends-Indep:
 python3-coverage,
 python3-hacking,
 python3-openstackdocstheme,
 python3-oslotest,
 python3-prettytable,
 python3-stestr,
 python3-testtools,
 subunit,
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/openstack-team/libs/python-automaton
Vcs-Git: https://salsa.debian.org/openstack-team/libs/python-automaton.git
Homepage: https://github.com/openstack/automaton

Package: python-automaton-doc
Section: doc
Architecture: all
Depends:
 ${misc:Depends},
 ${python3:Depends},
 ${sphinxdoc:Depends},
Suggests:
 python-automaton-doc,
Description: friendly state machines - Python 3.x
 Automaton is a friendly state machines for Python. This state machine can be
 used to automatically run a given set of transitions and states in response to
 events (either from callbacks or from generator/iterator send() values, see
 PEP 342). On each triggered event, a on_enter and on_exit callback can also be
 provided which will be called to perform some type of action on leaving a
 prior state and before entering a new state.
 .
 This package contains the documentation.

Package: python3-automaton
Architecture: all
Depends:
 python3-debtcollector,
 python3-pbr,
 python3-prettytable,
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 python-automaton-doc,
Description: friendly state machines - Python 3.x
 Automaton is a friendly state machines for Python. This state machine can be
 used to automatically run a given set of transitions and states in response to
 events (either from callbacks or from generator/iterator send() values, see
 PEP 342). On each triggered event, a on_enter and on_exit callback can also be
 provided which will be called to perform some type of action on leaving a
 prior state and before entering a new state.
 .
 This package contains the Python 3.x module.
